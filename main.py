# SPDX-FileCopyrightText: 2023 Egor Guslyancev <electromagneticcyclone@disroot.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"Executable with autoupdate. Only runs bot module."

import os
import signal
import time
import subprocess
from sys import stderr


class Runner:
    "Runs optimized bot script."
    __proc = None

    def stop(self):
        "Stop the script."
        os.kill(self.__proc.pid, signal.SIGTERM)

    def start(self):
        "Start the script."
        self.__proc = subprocess.Popen(["python", "-OO", "bot.py"])


bot_runner = Runner()


if __name__ == "__main__":
    branch = os.popen("git rev-parse --abbrev-ref HEAD").read()[:-1]
    bot_runner.start()
    while True:
        stderr.write("main.py: Updater\n")
        os.system("git fetch --quiet")
        if (
            os.popen("git log " + branch + "..origin/" + branch + " --oneline")
            .read()
            .strip()
            != ""
        ):
            stderr.write("main.py: Updating from git\n")
            bot_runner.stop()
            os.system("git pull --quiet")
            os.system("git gc --quiet --aggressive --prune=all")
            bot_runner.start()
        time.sleep(60 * 60 * 1)
