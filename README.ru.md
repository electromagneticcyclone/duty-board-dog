<!--
SPDX-FileCopyrightText: 2023 Egor Guslyancev <electromagneticcyclone@disroot.org>

SPDX-License-Identifier: Unlicense
-->

# Дежурик

[![REUSE status](https://api.reuse.software/badge/git.disroot.org/electromagneticcyclone/duty-board-dog)](https://api.reuse.software/info/git.disroot.org/electromagneticcyclone/duty-board-dog)

Телеграм бот для отслеживания дежурств. Есть админка, конфигурация расписания с недельными фазами, отслеживание дежурящих по ID, а не никам.

## Запуск

```python
git clone --no-tags --depth=1 --filter=tree:0 https://git.disroot.org/electromagneticcyclone/duty-board-dog.git
cd duty-board-dog
python -m pip install -r requirements.txt
cp config.example.ini config.ini
# Редактируем конфиг
python main.py
```

## Использование

1. Приглашаем бота на форум (телеграм группа с комнатами).
2. Пишем команду `/link`.
3. Смотрим чек-лист того, что нужно сделать дальше.

## Донат

![Пончик](assets/donut.png)
