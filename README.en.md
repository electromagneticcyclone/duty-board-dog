<!--
SPDX-FileCopyrightText: 2023 Egor Guslyancev <electromagneticcyclone@disroot.org>

SPDX-License-Identifier: Unlicense
-->

# Duty board dog

[![REUSE status](https://api.reuse.software/badge/git.disroot.org/electromagneticcyclone/duty-board-dog)](https://api.reuse.software/info/git.disroot.org/electromagneticcyclone/duty-board-dog)

Telegram bot for duty tracking. There is an admin control, configuration of schedule with weekly phases, tracking of people on duty by ID, not nicknames.

Untranslated into English.

## Starting

```python
git clone --no-tags --depth=1 --filter=tree:0 https://git.disroot.org/electromagneticcyclone/duty-board-dog.git
cd duty-board-dog
python -m pip install -r requirements.txt
cp config.example.ini config.ini
# Edit the config
python main.py
```

## Usage

1. Invite bot to your forum (telegram group with rooms).
2. Write the command `/link`.
3. Look at the check list.

## Donut

![Donut](assets/donut.png)
