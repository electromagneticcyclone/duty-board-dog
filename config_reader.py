# SPDX-FileCopyrightText: 2023 Egor Guslyancev <electromagneticcyclone@disroot.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"The module reads and checks config."

import configparser

FIELDS = {
    "tokens.prod": (str, "" if __debug__ else None, None),
    "tokens.devel": (str, None if __debug__ else "", None),
    "settings.notify_period": (int, 120, None),
}

CONFIG_FILENAME = "config.ini"
config = configparser.ConfigParser()
config.read(CONFIG_FILENAME)


def check_field(field: str) -> None:
    "Checks if the config field does exist and has the correct type."
    read(field)


def read(field: str) -> any:
    "Reads field and converts the value to the correct type."
    a = config
    if field not in FIELDS:
        raise ValueError(f"Field {field} does not exist.")
    t, d, allowed = FIELDS[field]
    for i in field.split("."):
        a = a[i]
    if a is None:
        if d is None:
            raise ValueError(f"Field {field} does not have value.")
        return d
    a = t(a)
    if allowed is not None:
        if a not in allowed:
            raise ValueError(f"Field {field} does not have allowed value.")
    return a


for f in FIELDS:
    check_field(f)
